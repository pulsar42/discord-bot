FROM node:9

RUN mkdir /opt/discordbot
WORKDIR /opt/discordbot
COPY . /opt/discordbot

CMD ["node", "dist/index.js"]