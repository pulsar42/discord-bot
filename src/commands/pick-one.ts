import * as Discord from "discord.js";

export class PickOneCommand {
    constructor(protected message: Discord.Message) { };

    public async command() {
        try {
            // Get our params from the user;
            const params = this.findParams();
            await this.message.channel.send(
                `[BOT] Okay. Je génère un chiffre aléatoire entre ${params.min} et ${params.max} ${params.exclude ? `(en excluant le chiffre ${params.exclude})` : ''}`
            )

            const randomNumber = this.generateRandomNumber(params);
            await this.message.channel.send(
                `[BOT] Le numéro généré est : **${randomNumber}**`
            );

        } catch (e) {
            console.error(e, { message: this.message });
            await this.message.channel.send(`[BOT] ${e.message}`);
        }
    }

    /**
     * Find the parameters for ourselves
     */
    protected findParams() {
        const regex = /^!cabot pick ([0-9]+) ([0-9]+)( ([0-9]+))?/;
        const m = regex.exec(this.message.cleanContent);

        if (!m || !m[0]) throw new Error(`Cette commande est invalide`);

        const min = Number(m[1]);
        const max = Number(m[2]);
        const exclude = m[4] ? Number(m[4]) : null;

        if (isNaN(min) || isNaN(max) || (exclude && isNaN(exclude))) throw new Error(`Chiffres invalides`);
        if (min <= 0) throw new Error('La borne minimale doit être strictement supérieur à 0')
        if (max <= min) throw new Error('La borne maximale doit être strictement supérieur à la borne minimale');


        return { min, max, exclude };
    }

    /**
     * Generate a random number from the given parameters
     * @param params 
     */
    protected generateRandomNumber(params: { min: number, max: number, exclude: number | null }) {
        let notFound: boolean = true;
        let random: number | null = null;

        while (notFound) {
            random =  Math.floor(Math.random() * (params.max - params.min + 1) + params.min);
            if (random !== params.exclude) notFound = false;
        }

        if (!random) throw new Error(`Impossible de générer un nombre aléatoire...`);
        return random;
    }
}