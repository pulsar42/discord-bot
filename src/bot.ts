import { PickOneCommand } from './commands/pick-one';
import * as Discord from "discord.js";

/**
 * Main class for launching our bot
 */
export class DiscordBot {

    protected _client: Discord.Client = new Discord.Client()

    constructor() {
        if (!process.env.DISCORD_TOKEN) throw new Error(`No Discord Token`);

        this._client.on("ready", () => {
            console.log(`Bot ready`);
        })

        this._client.on("message", async (message) => {
            // Do nothing if this is not a command for us
            if (message.cleanContent.indexOf("!cabot") !== 0) return;

            // Do the pick command
            if (message.cleanContent.indexOf("!cabot pick") === 0) {
                const cmd = new PickOneCommand(message);
                await cmd.command();
            }
        });

        this._client.login(process.env.DISCORD_TOKEN);
    }



}